// Gmsh project
ref1 = 0.5 ;
Point(1) = {0, 0, 0, ref1};
Point(2) = {80, 0, 0, ref1};
Point(3) = {80, 20, 0, ref1};
Point(4) = {0, 20, 0, ref1};
Line(1) = {4, 1};
Line(2) = {2, 3};
Line(3) = {1, 2};
Line(4) = {3, 4};
Line Loop(5) = {-1, -4, -2, -3};
Plane Surface(5) = {5};
Physical Line(1) = {1};
Physical Line(2) = {2};
Physical Line(3) = {3};
Physical Line(4) = {4};
Physical Surface(5) = {5};
