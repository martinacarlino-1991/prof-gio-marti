.SUFFIXES: .f90 .o .mod

PROGRAM     = run 
FC          = gfortran
CC          = gcc
COPT        = -O3
POPT        = -fopenmp
FIX         =

OBJS = \
aero.o\
erone.o \
output_setting.o \
BC_unstructured.o \
dump_in.o \
dump_out.o \
setup.o \
initialization.o \
mesh.o \
unstructured.o \
shearstress_un.o \
output_paraview_unstructured.o \
mass_check.o \
main.o \

all	: compile link
compile : shared.o $(OBJS)

$(OBJS) : 
.f90.o  :
	$(FC) $(POPT) -c $(COPT) $(FIX) $<
.mod.o  :
	$(FC) $(POPT) -c $(COPT) $(FIX) $<

link    :
	$(FC) $(POPT) $(FIX) -o $(PROGRAM) shared.o $(OBJS)

rmobj   :
	rm -f *.o *.mod *.vts *.vtu *.vtm *.gz dumpFile.dat log.dat parameters.dat CpCf.dat info_* run

remake  : rmobj  all
clean   :
	rm -f *.o *.mod
