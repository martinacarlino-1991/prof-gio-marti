 subroutine initialization

 use shared

 do i = 1,nNodes

    velt(1,i) = u0
    velt(2,i) = v0
    rhot(i)   = rho0

    do j = 1,9
       scont = c_ret(1,j)*velt(1,i)+c_ret(2,j)*velt(2,i)
       qcont = velt(1,i)**2.d0+velt(2,i)**2.d0

       if (feq_cubic .eq. 1) then
          fpop(j,i) = w_eq(j)*rho0*(1.d0+3.d0*scont+4.5d0*scont*scont-1.5d0*qcont+&
                    & 4.5d0*scont*scont*scont-4.5d0*scont*qcont)
       else
          fpop(j,i) = w_eq(j)*rho0*(1.d0+3.d0*scont+4.5d0*scont*scont-1.5d0*qcont)
       end if
    end do

 end do

 fneq_un = 0.d0

 do i = 1,nNodes
    sxx(i) = 0.d0
    sxy(i) = 0.d0
    syx(i) = 0.d0
    syy(i) = 0.d0
 end do

 end subroutine initialization
