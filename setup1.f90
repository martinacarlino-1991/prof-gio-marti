 subroutine setup

 use shared

 open(unit=112,file='input.dat',status='old')
    ! simulation parameters
    read(112,*)    id, iter
    read(112,*)    id, noutput
    read(112,*)    id, dump
    ! unstructured parameters
    read(112,*)    id, deltat
    read(112,*)    id, ril
    ! fluid flow parameters
    read(112,*)    id, Re
    read(112,*)    id, rho0
    read(112,*)    id, g
    ! mesh parameters
    read(112,*)    id, meshName
    read(112,*)    id, AddBuffer
    read(112,*)    id, buffer
    ! boundary conditions
    read(112,*)    id, inletBC_un
    read(112,*)    id, outletBC_un
    read(112,*)    id, TopWallBC_un
    read(112,*)    id, BottomWallBC_un
    ! schemes
    read(112,*)    id, feq_cubic
    ! characteristic length
    read(112,*)    id, length
    ! aerodynamic forces computation
    read(112,*)    id, obstacle
    read(112,*)    id, ncdrag
 close(112)

 ! velocità del suono
 cs = sqrt(1.d0/3.d0)
 csq = 3.d0

 ! kinematic viscosity
 kvisc = cs**2.d0*ril

 inv_ril = 1.d0/ril

 if (deltat .ge. 2.d0*ril) then
    write(*,*) '================================================'
    write(*,*) 'WARNING! CFL stability constraint not satisfied!'
    write(*,*) '================================================'
 end if

 ! characteristic velocity
 u0 = Re*kvisc/length
 v0 = 0.d0

 ! Mach number
 Mach = u0/cs

 if (u0 .ge. cs/3.d0) then
    write(*,*) '=============================='
    write(*,*) 'WARNING! Mach number too high!'
    write(*,*) '=============================='
 end if

 open(unit=300,file='parameters.dat')
 write(300,"(A,F8.4)") 'tau_unstructured:                             ' , ril
 write(300,"(A,F8.4)") 'kinematic viscosity:                          ' , kvisc
 write(300,"(A,F8.4)") 'delta_t unstructured:                         ' , deltat
 write(300,"(A,F8.2)") 'characteristic length:                        ' , length
 write(300,"(A,F8.4)") 'inflow velocity:                              ' , u0
 write(300,"(A,F8.2)") 'Reynolds number:                              ' , Re
 write(300,"(A,F8.4)") 'Mach number:                                  ' , Mach
 close(300)
 
 rinv_9  = 1.d0/9.d0
 rinv_12 = 1.d0/12.d0
 
 ! fattore moltiplicativo delle feq
 w_eq(1) = 4.d0/9.d0
 w_eq(2) = 1.d0/9.d0
 w_eq(3) = 1.d0/9.d0
 w_eq(4) = 1.d0/9.d0
 w_eq(5) = 1.d0/9.d0
 w_eq(6) = 1.d0/36.d0
 w_eq(7) = 1.d0/36.d0
 w_eq(8) = 1.d0/36.d0
 w_eq(9) = 1.d0/36.d0

 ! velocità di reticolo
 c_ret(1,1) =  0.d0 ; c_ret(2,1) =  0.d0
 c_ret(1,2) =  1.d0 ; c_ret(2,2) =  0.d0
 c_ret(1,3) =  0.d0 ; c_ret(2,3) =  1.d0
 c_ret(1,4) = -1.d0 ; c_ret(2,4) =  0.d0
 c_ret(1,5) =  0.d0 ; c_ret(2,5) = -1.d0
 c_ret(1,6) =  1.d0 ; c_ret(2,6) =  1.d0
 c_ret(1,7) = -1.d0 ; c_ret(2,7) =  1.d0
 c_ret(1,8) = -1.d0 ; c_ret(2,8) = -1.d0
 c_ret(1,9) =  1.d0 ; c_ret(2,9) = -1.d0

 ! vettore per il calcolo del tensore degli sforzi
 do i = 1,9
    qxx(i) = c_ret(1,i)**2.d0
    qyy(i) = c_ret(2,i)**2.d0
    qxy(i) = c_ret(1,i)*c_ret(2,i)
    qyx(i) = c_ret(2,i)*c_ret(1,i)
 end do

 call mesh

 ! y-coordinate max and min values evaluation
 Y_min = minval(yn)
 Y_max = maxval(yn)

 ! high of the channel
 h = abs(Y_max-Y_min)

 !========================== UNSTRUCTURED variables ==========================!
 allocate(feq(1:9,1:nNodes), fneq_un(1:9,1:nNodes))
 allocate(fpop(1:9,1:nNodes), rhot(1:nNodes), velt(1:2,1:nNodes))
 allocate(fpop_0(1:9,1:nNodes), rhot_0(1:nNodes), velt_0(1:2,1:nNodes))
 allocate(c_cd(1:nNodes), c_sd(1:9,1:nNodes), c_c(1:maxc,1:nNodes), c_s(1:maxc,1:9,1:nNodes))
 allocate(xvc(1:4,1:nElements), yvc(1:4,1:nElements))
 allocate(norm(1:2,1:3,1:nElements), normtr(1:2,1:3,1:nElements))
 allocate(surf(1:3,1:2,1:nElements))
 allocate(lengthl(1:3,1:nElements), lengtht(1:3,1:nElements), lengthr(1:3,1:nElements))
 allocate(surft(1:nNodes),surf_inv(1:nNodes))
 allocate(sxx(1:nNodes), sxy(1:nNodes), syx(1:nNodes), syy(1:nNodes))
 allocate(connections(1:nNodes,1:maxc), nnodi(1:nNodes))
 if (obstacle .eq. 1) then
    allocate(lift_p(1:nObstacle),lift_v(1:nObstacle),drag_p(1:nObstacle),drag_v(1:nObstacle))
    allocate(CP(1:nObstacle),CF(1:nObstacle))
 end if
 !================ setup for Paraview (.vtu) post-processing =================!
 allocate(cn(1:nElements))                                ! cell numbers
 allocate(off(1:nElements))                               ! offsets for cells
 allocate(tp(1:nElements))                                ! types for cells
 !============================================================================!
 do i = 1,nElements
    cn(i) = i
 end do

 off(1) = 3
 do i = 2,nElements
    off(i) = off(i-1)+3
 end do

 do i = 1,nElements
    tp(i) = 5
 end do
 !============================================================================!

 ! unstructured parameters definition 

 nnodi = 0           ! nnodi:       numero di nodi vicini al nodo considerato
 connections = 0     ! connections: lista dei nodi vicini al nodo considerato

 do i = 1,nElements
    ! allocazione delle coordinate dei nodi in matrice locale
    do j = 1,3
       ncon(j) = conn_tot(j,i)
       xloc(j) = x(ncon(j))
       yloc(j) = y(ncon(j))
    end do
    ! calcolo delle coordinate x,y dei nodi del volume di controllo
    xvc(1,i) = (xloc(1)+xloc(2))*0.5d0
    xvc(2,i) = (xloc(2)+xloc(3))*0.5d0
    xvc(3,i) = (xloc(1)+xloc(3))*0.5d0
    xvc(4,i) = (xloc(1)+xloc(2)+xloc(3))/3.d0
    yvc(1,i) = (yloc(1)+yloc(2))*0.5d0
    yvc(2,i) = (yloc(2)+yloc(3))*0.5d0
    yvc(3,i) = (yloc(1)+yloc(3))*0.5d0
    yvc(4,i) = (yloc(1)+yloc(2)+yloc(3))/3.d0
    ! calcolo delle lunghezze dei lati dei triangoli componenti il volume di controllo
    do j = 1,3
       lengthl(j,i) = sqrt(((xvc(4,i)-xvc(j,i))**2.d0)+((yvc(4,i)-yvc(j,i))**2.d0))
       lengthr(j,i) = sqrt(((xvc(4,i)-xloc(j))**2.d0)+((yvc(4,i)-yloc(j))**2.d0))
       lengtht(j,i) = sqrt(((xvc(j,i)-xloc(j))**2.d0)+((yvc(j,i)-yloc(j))**2.d0))
    end do
    ! calcolo dei versori normali ai lati del volume di controllo
    do j = 1,3
       norm (1,j,i) = -(yvc(4,i)-yvc(j,i))/lengthl(j,i)
       norm (2,j,i) =  (xvc(4,i)-xvc(j,i))/lengthl(j,i)
    end do
    ! calcolo dei versori normali ai lati dell'elemento, entrante nell'elemento
    do j = 1,3
       normtr (1,j,i) =  (yvc(j,i)-yloc(j))/lengtht(j,i)
       normtr (2,j,i) = -(xvc(j,i)-xloc(j))/lengtht(j,i)
    end do
    ! calcolo delle superfici
    do j = 1,3
       lato_a = lengthl(j,i)
       lato_b = lengthr(j,i)
       lato_c = lengtht(j,i)
       call erone
       surf(j,1,i) = sup
    end do
       lato_a = lengthl(3,i)
       lato_b = lengthr(1,i)
       lato_c = lengtht(3,i)
       call erone
       surf(1,2,i) = sup
    do j = 2,3
       lato_a = lengthl(j-1,i)
       lato_b = lengthr(j,i)
       lato_c = lengtht(j-1,i)
       call erone
       surf(j,2,i) = sup
    end do
    do j = 1,3
       surft(ncon(j)) = surft(ncon(j))+surf(j,1,i)+surf(j,2,i)
    end do
    ! calcolo matrici di streaming e collision
    do j = 1,3
       c_cd(ncon(j)) = c_cd(ncon(j))-rinv_9*5.5d0*(surf(j,1,i)+surf(j,2,i))
       if     (j .eq. 1) then
          n1 = ncon(1)
          n2 = ncon(2)
          n3 = ncon(3)
          k1 = 1
          k2 = 2
          k3 = 3
       elseif (j .eq. 2) then
          n1 = ncon(2)
          n2 = ncon(3)
          n3 = ncon(1)
          k1 = 2
          k2 = 3
          k3 = 1
       elseif (j .eq. 3) then
          n1 = ncon(3)
          n2 = ncon(1)
          n3 = ncon(2)
          k1 = 3
          k2 = 1
          k3 = 2
       endif

       do k = 1,9
          c_sd(k,n1) = c_sd(k,n1)+5.d0*rinv_12*(lengthl(k1,i)*(norm(1,k1,i)*c_ret(1,k)+norm(2,k1,i)*c_ret(2,k))- &
     &    lengthl(k3,i)*(norm(1,k3,i)*c_ret(1,k)+norm(2,k3,i)*c_ret(2,k)))
       end do

!      CASO 1-2

       do l = 1,nnodi(n1)
          if ( connections(n1,l) .eq. n2) exit
       end do
       if (connections(n1,l) .ne. n2) then
          nnodi(n1) = nnodi(n1)+1
          l = nnodi(n1)
          connections(n1,l) = n2
       end if
       c_c(l,n1) = c_c(l,n1)-rinv_9*(2.5d0*surf(k1,1,i)+surf(k1,2,i))

       do k = 1,9
          c_s(l,k,n1) = c_s(l,k,n1)+rinv_12*(5.d0*lengthl(k1,i)*(norm(1,k1,i)*c_ret(1,k)+norm(2,k1,i)* &
        & c_ret(2,k))-2.d0*lengthl(k3,i)*(norm(1,k3,i)*c_ret(1,k)+norm(2,k3,i)*c_ret(2,k)))

!      CONDIZIONE AL CONTORNO PARETE-COVOLUME
          if (flag(n1) .ge. 2 .and. flag(n2) .ge. 2) then
             nr1 = -(y(n2)-y(n1))*0.5d0
             nr2 =  (x(n2)-x(n1))*0.5d0
             r_nc = (nr1*c_ret(1,k)+nr2*c_ret(2,k))
             c_s(l,k,n1) = c_s(l,k,n1)+0.25d0*r_nc
             c_sd(k,n1)  = c_sd(k,n1 )+0.75d0*r_nc
          end if
       end do

!      CASO 2-1

       do l = 1,nnodi(n2)
          if (connections(n2,l) .eq. n1) exit
       enddo
       if (connections(n2,l) .ne. n1) then
          nnodi(n2) = nnodi(n2)+1
          l = nnodi(n2)
          connections(n2,l) = n1
       end if
       c_c(l,n2) = c_c(l,n2)-rinv_9*(2.5d0*surf(k2,2,i)+surf(k2,1,i))

       do k = 1,9
          c_s(l,k,n2) = c_s(l,k,n2)-rinv_12*(5.d0*lengthl(k1,i)*(norm(1,k1,i)*c_ret(1,k)+norm(2,k1,i)*&
        & c_ret(2,k))-2.d0*lengthl(k2,i)*(norm(1,k2,i)*c_ret(1,k)+norm(2,k2,i)*c_ret(2,k)))

!      CONDIZIONE AL CONTORNO PARETE-COVOLUME
          if (flag(n1) .ge. 2 .and. flag(n2) .ge. 2) then
             nr1 = -(y(n2)-y(n1))*0.5d0
             nr2 =  (x(n2)-x(n1))*0.5d0
             r_nc = (nr1*c_ret(1,k)+nr2*c_ret(2,k))
             c_s(l,k,n2) = c_s(l,k,n2)+0.25d0*r_nc
             c_sd(k,n2)  = c_sd(k,n2 )+0.75d0*r_nc
          end if
       end do

    end do 
 end do

 do i =1,nNodes
    surf_inv(i) = deltat/surft(i)
 end do

 end subroutine setup
