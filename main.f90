 program main

 use shared
 use omp_lib

 call omp_set_num_threads(n_th)

 call setup

 if (dump .eq. 0) then
    it  = 0
    it0 = 1
    call initialization
    if (ncdrag .eq. 1) then
       open(unit=150,file='log.dat')
       write(150,*) 'iteration','    ','err %','       ','Cd','        ','Cl'
       write(150,*) '========================================='
       close(150)
    else
       open(unit=152,file='log.dat')
       write(152,*) 'iteration','    ','err %'
       write(152,*) '========================'
       close(152)
    end if 
 else
    call dump_in
    it0 = it
 end if

 call output_setting

 do it = it0,iter

    if (it == it0) then
      write(*,*), it
    end if 
 
    call unstructured

    do i = 1,nNodes
       call BC_unstructured
    end  do

    call mass_check

    if (it == itOut .or. it == iter) then

       if (ncdrag .eq. 1) then
          call shearstress_un
          call aero
          open(unit=150,file='log.dat',position='append')
          write(150,"(I8,A,F10.4,A,2F10.4)") it,' ',errInOut,' ',CD,CL
          close(150)
          open(unit=151,file='CpCf.dat',position='append')
          do i = 1,nObstacle
             write(151,*) CP(i), CF(i)
          end do
          close(151)
       else
          open(unit=152,file='log.dat',position='append')
          write(152,"(I8,A,F10.4)") it,' ',errInOut
          close(152)
       end if 
 
       call dump_out
       call output_paraview_unstructured

       write(*,*) it, errInOut

    end if

 end do

 end program main
