 subroutine mass_check

 use shared

 rmass = 0.d0
 do i = 1,nNodes
    rmass = rmass+rhot(i)
 end do
 errTOT = abs(rmass-nNodes)/nNodes*100

 massIn = 0.d0
 do i = 1,nInlet
    j = inletBufferNodes(i)
    massIn = massIn + rhot(j)*velt(1,j)
 end do
 massOut = 0.d0
 do i = 1,nOutlet
    j = outletBufferNodes(i)
    massOut = massOut + rhot(j)*velt(1,j)
 end do
 errInOut = abs(massIn-massOut)/massIn*100

 end subroutine mass_check
