 subroutine dump_in

!========================!
! Restart initialization !
!========================!

 use shared

 rhot = 0.d0
 velt = 0.d0

 open(unit=90,file='dumpFile.dat')

 read(90,*) it

 do i = 1,nNodes
    do j = 1,9
       read(90,*) fpop(j,i)
   end do
 end do

 do i = 1,nNodes
    do j = 1,9
       rhot(i) = fpop(j,i)+rhot(i)
    end do
 end do

 do i = 1,nNodes
    do j = 1,9
       do k = 1,2
          velt(k,i) = velt(k,i)+(1.d0/rhot(i))*(c_ret(k,j)*fpop(j,i))
       end do
    end do
 end do

 close(90)

 end subroutine dump_in
