  subroutine output_paraview_unstructured

  use shared
  
  write(fn,*), it
  filename_1 = 'out_'//trim(adjustl(fn))//'.vtu'
  filename = trim(adjustl(filename_1))

  write(NN,*), nNodes
  write(NC,*), nElements

!========================== offset variables ==========================!

  ss1 = nNodes*sizeof(sizereal)
  ss2 = nElements*sizeof(sizeint)

  offset_velocity     = 0
  offset_density      = offset_velocity     + sizeof(sizeint) + 2*ss1
  offset_shear        = offset_density      + sizeof(sizeint) +   ss1
  offset_cell_number  = offset_shear        + sizeof(sizeint) + 3*ss1
  offset_coordinates  = offset_cell_number  + sizeof(sizeint) +   ss2
  offset_connectivity = offset_coordinates  + sizeof(sizeint) + 3*ss1
  offset_offsets      = offset_connectivity + sizeof(sizeint) + 3*ss2
  offset_types        = offset_offsets      + sizeof(sizeint) +   ss2

  write(velocity,    *), offset_velocity
  write(density,     *), offset_density
  write(shear,       *), offset_shear
  write(cell_number, *), offset_cell_number
  write(coordinates, *), offset_coordinates
  write(connectivity,*), offset_connectivity
  write(offsets,     *), offset_offsets
  write(types,       *), offset_types

!======================================================================!

  open(unit=90,file='part_1')
     string = '<?xml version="1.0"?>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">'
     write(90,"(A)"),trim(adjustl(string))
     string = '<UnstructuredGrid>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<Piece NumberOfPoints=" '//trim(adjustl(NN))//' " NumberOfCells=" '//trim(adjustl(NC))//' ">'
     write(90,"(A)"),trim(adjustl(string))
!=======================================================================================================!
     string = '<PointData>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Float64" Name="velocity" NumberOfComponents="2" format="appended" &
	      offset="'//trim(adjustl(velocity))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Float64" Name="density" NumberOfComponents="1" format="appended" &
	      offset="'//trim(adjustl(density))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Float64" Name="shear" NumberOfComponents="3" format="appended" &
	      offset="'//trim(adjustl(shear))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '</PointData>'
     write(90,"(A)"),trim(adjustl(string))
!=======================================================================================================!
     string = '<CellData Scalars="scalars">'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Int32" Name="cell_number" format="appended" &
	      offset="'//trim(adjustl(cell_number))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '</CellData>'
     write(90,"(A)"),trim(adjustl(string))
!=======================================================================================================!
     string = '<Points>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Float64" NumberOfComponents="3" format="appended" &
	      offset="'//trim(adjustl(coordinates))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '</Points>'
     write(90,"(A)"),trim(adjustl(string))
!=======================================================================================================!
     string = '<Cells>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Int32" Name="connectivity" format="appended" &
	      offset="'//trim(adjustl(connectivity))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Int32" Name="offsets" format="appended" &
	      offset="'//trim(adjustl(offsets))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<DataArray type="Int32" Name="types" format="appended" &
	      offset="'//trim(adjustl(types))//'"/>'
     write(90,"(A)"),trim(adjustl(string))
     string = '</Cells>'
     write(90,"(A)"),trim(adjustl(string))
!=======================================================================================================!
     string = '</Piece>'
     write(90,"(A)"),trim(adjustl(string))
     string ='</UnstructuredGrid>'
     write(90,"(A)"),trim(adjustl(string))
     string = '<AppendedData encoding="raw">'
     write(90,"(A)"),trim(adjustl(string))
  close(90)
  open(unit=90,file='part_2',form='unformatted',access='stream')
     write(90),'_',2*ss1
        do i = 1,nNodes
           write(90),velt(1,i),velt(2,i)
        enddo
     write(90),ss1
        do i = 1,nNodes
           write(90),rhot(i)
        enddo
     write(90),3*ss1
        do i = 1,nNodes
           write(90),sxx(i),sxy(i),syy(i)
        enddo
     write(90),ss2
        do i = 1,nElements
           write(90),cn(i)
        enddo
     write(90),3*ss1
        do i = 1,nNodes
           write(90),x(i),y(i),z(i)
        enddo

     ! in Paraview, la numerazione dei nodi nella matrice di connettività deve partire da 0
     ! quindi sottraggo 1 alla numerazione data da Gmsh
     write(90),3*ss2
        do i = 1,nElements
           write(90),conn_tot(1,i)-1,conn_tot(2,i)-1,conn_tot(3,i)-1
        enddo

     write(90),ss2   
        do i = 1,nElements
           write(90),off(i)
        enddo
     write(90),ss2
        do i = 1,nElements
           write(90),tp(i)
        enddo
  close(90)
  open(unit=90,file='part_3')
     write(90,*)
     string = '</AppendedData>'
     write(90,"(A)"),trim(adjustl(string))
     string = '</VTKFile>'
     write(90,"(A)"),trim(adjustl(string))
  close(90)

  command = 'cat '//trim(adjustl('part_1'))//' '//trim(adjustl('part_2'))//' '//trim(adjustl('part_3'))//' > '//filename
  call system(command)
  command = 'rm -f part_1 part_2 part_3'
  call system(command)
  command = 'gzip -9 '//filename
  call system(command)

  if (it == itOut) then
     itOut = itOut + deltaOut
  end if

  end subroutine output_paraview_unstructured
