 subroutine shearstress_un
 
 use shared

 do i = 1,nNodes

    sxx(i) = 0.d0
    syy(i) = 0.d0
    sxy(i) = 0.d0
    syx(i) = 0.d0

    do j = 1,9

!       sxx(i) = sxx(i)-fneq_un(j,i)*(qxx(j)-cs**2.d0)
!       syy(i) = syy(i)-fneq_un(j,i)*(qyy(j)-cs**2.d0)
!       sxy(i) = sxy(i)-fneq_un(j,i)*qxy(j)
!       syx(i) = syx(i)-fneq_un(j,i)*qyx(j)

       sxx(i) = sxx(i)-fneq_un(j,i)*qxx(j)
       syy(i) = syy(i)-fneq_un(j,i)*qyy(j)
       sxy(i) = sxy(i)-fneq_un(j,i)*qxy(j)
       syx(i) = syx(i)-fneq_un(j,i)*qyx(j)

    end do

 end do

 end subroutine shearstress_un
