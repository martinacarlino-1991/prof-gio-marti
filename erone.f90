 subroutine erone

!===============!
! Triangle area !
!===============!

 use shared

 semip = (lato_a+lato_b+lato_c)*0.5d0
 sup   = sqrt(abs(semip*(semip-lato_a)*(semip-lato_b)*(semip-lato_c)))

 end subroutine erone
