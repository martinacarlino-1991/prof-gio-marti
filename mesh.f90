subroutine mesh

 use shared

!=================================================================================================================!
!                                              CONVENZIONE MESH                                                   !
!=================================================================================================================!
!                                                                                                                 !
!  ELEMENTI                                                                                                       !
!                                                                                                                 !
!  Nella matrice di connettività, i nodi di ogni elemento sono letti in senso 'orario'.                           !
!-----------------------------------------------------------------------------------------------------------------!
!  NODI                                                              FLAG                                         !
!                          top  wall                                                                              !
!                           (tag:4)                                                                               !
!                                                                                                                 !
!                 x|4 o o o o o o o o o o 3|x                        5|7 7 7 7  7  7  7  7  7 7 7 7|6             !
!             |   x|o o o o o o o o o o o o|x  Λ                     2|0 0 0 0  0  0  0  0  0 0 0 0|3             !
!  inlet      |   x|o o o o o * * o o o o o|x  |    outlet           2|0 0 0 0  0 10 10  0  0 0 0 0|3             !
! (tag:1)     |   x|o o o o *     * o o o o|x  |    (tag:2)          2|0 0 0 0 10       10  0 0 0 0|3             !
!             V   x|o o o o o * * o o o o o|x  |                     2|0 0 0 0  0 10 10  0  0 0 0 0|3             !
!                 x|o o o o o o o o o o o o|x                        2|0 0 0 0  0  0  0  0  0 0 0 0|3             !
!                 x|1 o o o o o o o o o o 2|x                        5|4 4 4 4  4  4  4  4  4 4 4 4|6             !
!                                                                                                                 !
!                         bottom wall                                                                             !
!                           (tag:3)                                                                               !
!                                                                    domain   :  0                                !
!  Le frecce indicano l'ordine di lettura dei nodi                   inlet    :  2  ;  corner inlet  : 5          !
!  di inlet e outlet (inlet: da 4 a 1 ; outlet: da 2 a 3)            outlet   :  3  ;  corner outlet : 6          !
!                                                                    wall     :  4                                !
!  I nodi 'x' rappresentano i buffer                                 obstacle : 10                                !
!                                                                                                                 !
!=================================================================================================================!

!========== mesh file ==========!
 string = meshName
!===============================!

 open(unit=50,file='info_mesh.dat')
 str1 = 4
 str2 = 2
 open(unit=10,file=trim(adjustl(string)),status='old')
 do i = 1,str1
    read(10,*) nothing
 end do
 read(10,*) nNodes
 write(50,*) '==================================='
 write(50,*) 'number of nodes:'
 write(50,*) nNodes
 write(50,*) '==================================='
 allocate(xn(1:nNodes),yn(1:nNodes),zn(1:nNodes),flagn(1:nNodes))
 flagn = 0
 do i = 1,nNodes
    read(10,*) xxx,xn(i),yn(i),zn(i)
 end do
 do i = 1,str2
    read(10,*) nothing
 end do
 read(10,*) totElements
 do i = 1,totElements
    read(10,*) xxx, check
    if (check .eq. 2) exit
 end do
 close(10)
 nLines = xxx-1
 nElements = totElements - nLines
 write(50,*) 'number of elements:'
 write(50,*) nElements
 write(50,*)'==================================='
 allocate(conn(1:3,1:nElements))
 open(unit=10,file=trim(adjustl(string)),status='old')
 do i = 1,str1+1+nNodes+str2+1
    read(10,*) nothing
 end do
 nInlet = 0
 nOutlet = 0
 nWall_bottom = 0
 nWall_top = 0
 nObstacle = 0
 do i = 1,nLines
    read(10,*) xxx, xxx, xxx, tag, xxx, a, b
    if      (tag .eq.  1) then
       temp = 0
       if (nInlet .eq. 0) then
          nInlet = 2
          allocate(inletNodes(1:nInlet))
          allocate(temporaryIn(1:nInlet))
          inletNodes(1) = a
          inletNodes(2) = b
          temporaryIn = inletNodes
       else if (a .eq. tempIn) then
          do j = 1,nInlet
             if (b .eq. temporaryIn(j)) then
                temp = 1
                allocate(inletNodes(1:nInlet))
                exit
             end if
          end do
          if (temp .ne. 1) then
             nInlet = nInlet+1
             allocate(inletNodes(1:nInlet))
             do j = 1,nInlet-1
                inletNodes(j) = temporaryIn(j)
             end do
             inletNodes(nInlet) = b
             deallocate(temporaryIn)
             allocate(temporaryIn(1:nInlet))
             temporaryIn = inletNodes
          end if
       else if (a .ne. tempIn) then
          do jj = 1,nInlet
             if      (a .eq. temporaryIn(jj)) then
                nInlet = nInlet+1
                allocate(inletNodes(1:nInlet))
                do j = 1,nInlet-1
                   inletNodes(j) = temporaryIn(j)
                end do
                inletNodes(nInlet) = b
                deallocate(temporaryIn)
                allocate(temporaryIn(1:nInlet))
                temporaryIn = inletNodes
                temp =1
                exit
             else if (b .eq. temporaryIn(jj)) then
                nInlet = nInlet+1
                allocate(inletNodes(1:nInlet))
                do j = 1,nInlet-1
                   inletNodes(j) = temporaryIn(j)
                end do
                inletNodes(nInlet) = a
                deallocate(temporaryIn)
                allocate(temporaryIn(1:nInlet))
                temporaryIn = inletNodes
                temp = 1
                exit
             end if
          end do
          if (temp .ne. 1) then
             nInlet = nInlet+2
             allocate(inletNodes(1:nInlet))
             do j = 1,nInlet-2
                inletNodes(j) = temporaryIn(j)
             end do
             inletNodes(nInlet-1) = a
             inletNodes(nInlet)   = b
             deallocate(temporaryIn)
             allocate(temporaryIn(1:nInlet))
             temporaryIn = inletNodes
          end if
       end if
       tempIn = b
       deallocate(inletNodes)
    else if (tag .eq.  2) then
       temp = 0
       if (nOutlet .eq. 0) then
          nOutlet = 2
          allocate(outletNodes(1:nOutlet))
          allocate(temporaryOut(1:nOutlet))
          outletNodes(1) = a
          outletNodes(2) = b
          temporaryOut = outletNodes
       else if (a .eq. tempOut) then
          do j = 1,nOutlet
             if (b .eq. temporaryOut(j)) then
                temp = 1
                allocate(outletNodes(1:nOutlet))
                exit
             end if
          end do
          if (temp .ne. 1) then
             nOutlet = nOutlet+1
             allocate(outletNodes(1:nOutlet))
             do j = 1,nOutlet-1
                outletNodes(j) = temporaryOut(j)
             end do
             outletNodes(nOutlet) = b
             deallocate(temporaryOut)
             allocate(temporaryOut(1:nOutlet))
             temporaryOut = outletNodes
          end if
       else if (a .ne. tempOut) then
          do jj = 1,nOutlet
             if      (a .eq. temporaryOut(jj)) then
                nOutlet = nOutlet+1
                allocate(outletNodes(1:nOutlet))
                do j = 1,nOutlet-1
                   outletNodes(j) = temporaryOut(j)
                end do
                outletNodes(nOutlet) = b
                deallocate(temporaryOut)
                allocate(temporaryOut(1:nOutlet))
                temporaryOut = outletNodes
                temp =1
                exit
             else if (b .eq. temporaryOut(jj)) then
                nOutlet = nOutlet+1
                allocate(outletNodes(1:nOutlet))
                do j = 1,nOutlet-1
                   outletNodes(j) = temporaryOut(j)
                end do
                outletNodes(nOutlet) = a
                deallocate(temporaryOut)
                allocate(temporaryOut(1:nOutlet))
                temporaryOut = outletNodes
                temp = 1
                exit
             end if
          end do
          if (temp .ne. 1) then
             nOutlet = nOutlet+2
             allocate(outletNodes(1:nOutlet))
             do j = 1,nOutlet-2
                outletNodes(j) = temporaryOut(j)
             end do
             outletNodes(nOutlet-1) = a
             outletNodes(nOutlet)   = b
             deallocate(temporaryOut)
             allocate(temporaryOut(1:nOutlet))
             temporaryOut = outletNodes
          end if
       end if
       tempOut = b
       deallocate(outletNodes)
    else if (tag .eq.  3) then
       temp = 0
       if (nWall_bottom .eq. 0) then
          nWall_bottom = 2
          allocate(wallBottomNodes(1:nWall_bottom))
          allocate(temporaryWallB(1:nWall_bottom))
          wallBottomNodes(1) = a
          wallBottomNodes(2) = b
          temporaryWallB = wallBottomNodes
       else if (a .eq. tempWallB) then
          do j = 1,nWall_bottom
             if (b .eq. temporaryWallB(j)) then
                temp = 1
                allocate(wallBottomNodes(1:nWall_bottom))
                exit
             end if
          end do
          if (temp .ne. 1) then
             nWall_bottom = nWall_bottom+1
             allocate(wallBottomNodes(1:nWall_bottom))
             do j = 1,nWall_bottom-1
                wallBottomNodes(j) = temporaryWallB(j)
             end do
             wallBottomNodes(nWall_bottom) = b
             deallocate(temporaryWallB)
             allocate(temporaryWallB(1:nWall_bottom))
             temporaryWallB = wallBottomNodes
          end if
       else if (a .ne. tempWallB) then
          do jj = 1,nWall_bottom
             if      (a .eq. temporaryWallB(jj)) then
                nWall_bottom = nWall_bottom+1
                allocate(wallBottomNodes(1:nWall_bottom))
                do j = 1,nWall_bottom-1
                   wallBottomNodes(j) = temporaryWallB(j)
                end do
                wallBottomNodes(nWall_bottom) = b
                deallocate(temporaryWallB)
                allocate(temporaryWallB(1:nWall_bottom))
                temporaryWallB = wallBottomNodes
                temp =1
                exit
             else if (b .eq. temporaryWallB(jj)) then
                nWall_bottom = nWall_bottom+1
                allocate(wallBottomNodes(1:nWall_bottom))
                do j = 1,nWall_bottom-1
                   wallBottomNodes(j) = temporaryWallB(j)
                end do
                wallBottomNodes(nWall_bottom) = a
                deallocate(temporaryWallB)
                allocate(temporaryWallB(1:nWall_bottom))
                temporaryWallB = wallBottomNodes
                temp = 1
                exit
             end if
          end do
          if (temp .ne. 1) then
             nWall_bottom = nWall_bottom+2
             allocate(wallBottomNodes(1:nWall_bottom))
             do j = 1,nWall_bottom-2
                wallBottomNodes(j) = temporaryWallB(j)
             end do
             wallBottomNodes(nWall_bottom-1) = a
             wallBottomNodes(nWall_bottom)   = b
             deallocate(temporaryWallB)
             allocate(temporaryWallB(1:nWall_bottom))
             temporaryWallB = wallBottomNodes
          end if
       end if
       tempWallB = b
       deallocate(wallBottomNodes)
    else if (tag .eq.  4) then
       temp = 0
       if (nWall_top .eq. 0) then
          nWall_top = 2
          allocate(wallTopNodes(1:nWall_top))
          allocate(temporaryWallT(1:nWall_top))
          wallTopNodes(1) = a
          wallTopNodes(2) = b
          temporaryWallT = wallTopNodes
       else if (a .eq. tempWallT) then
          do j = 1,nWall_top
             if (b .eq. temporaryWallT(j)) then
                temp = 1
                allocate(wallTopNodes(1:nWall_top))
                exit
             end if
          end do
          if (temp .ne. 1) then
             nWall_top = nWall_top+1
             allocate(wallTopNodes(1:nWall_top))
             do j = 1,nWall_top-1
                wallTopNodes(j) = temporaryWallT(j)
             end do
             wallTopNodes(nWall_top) = b
             deallocate(temporaryWallT)
             allocate(temporaryWallT(1:nWall_top))
             temporaryWallT = wallTopNodes
          end if
       else if (a .ne. tempWallT) then
          do jj = 1,nWall_top
             if      (a .eq. temporaryWallT(jj)) then
                nWall_top = nWall_top+1
                allocate(wallTopNodes(1:nWall_top))
                do j = 1,nWall_top-1
                   wallTopNodes(j) = temporaryWallT(j)
                end do
                wallTopNodes(nWall_top) = b
                deallocate(temporaryWallT)
                allocate(temporaryWallT(1:nWall_top))
                temporaryWallT = wallTopNodes
                temp =1
                exit
             else if (b .eq. temporaryWallT(jj)) then
                nWall_top = nWall_top+1
                allocate(wallTopNodes(1:nWall_top))
                do j = 1,nWall_top-1
                   wallTopNodes(j) = temporaryWallT(j)
                end do
                wallTopNodes(nWall_top) = a
                deallocate(temporaryWallT)
                allocate(temporaryWallT(1:nWall_top))
                temporaryWallT = wallTopNodes
                temp = 1
                exit
             end if
          end do
          if (temp .ne. 1) then
             nWall_top = nWall_top+2
             allocate(wallTopNodes(1:nWall_top))
             do j = 1,nWall_top-2
                wallTopNodes(j) = temporaryWallT(j)
             end do
             wallTopNodes(nWall_top-1) = a
             wallTopNodes(nWall_top)   = b
             deallocate(temporaryWallT)
             allocate(temporaryWallT(1:nWall_top))
             temporaryWallT = wallTopNodes
          end if
       end if
       tempWallT = b
       deallocate(wallTopNodes)
    else if (tag .eq. 10) then
       temp = 0
       if (nObstacle .eq. 0) then
          nObstacle = 2
          allocate(obstacleNodes(1:nObstacle))
          allocate(temporaryOb(1:nObstacle))
          obstacleNodes(1) = a
          obstacleNodes(2) = b
          temporaryOb = obstacleNodes
       else if (a .eq. tempOb) then
          do j = 1,nObstacle
             if (b .eq. temporaryOb(j)) then
                temp = 1
                allocate(obstacleNodes(1:nObstacle))
                exit
             end if
          end do
          if (temp .ne. 1) then
             nObstacle = nObstacle+1
             allocate(obstacleNodes(1:nObstacle))
             do j = 1,nObstacle-1
                obstacleNodes(j) = temporaryOb(j)
             end do
             obstacleNodes(nObstacle) = b
             deallocate(temporaryOb)
             allocate(temporaryOb(1:nObstacle))
             temporaryOb = obstacleNodes
          end if
       else if (a .ne. tempOb) then
          do jj = 1,nObstacle
             if      (a .eq. temporaryOb(jj)) then
                nObstacle = nObstacle+1
                allocate(obstacleNodes(1:nObstacle))
                do j = 1,nObstacle-1
                   obstacleNodes(j) = temporaryOb(j)
                end do
                obstacleNodes(nObstacle) = b
                deallocate(temporaryOb)
                allocate(temporaryOb(1:nObstacle))
                temporaryOb = obstacleNodes
                temp =1
                exit
             else if (b .eq. temporaryOb(jj)) then
                nObstacle = nObstacle+1
                allocate(obstacleNodes(1:nObstacle))
                do j = 1,nObstacle-1
                   obstacleNodes(j) = temporaryOb(j)
                end do
                obstacleNodes(nObstacle) = a
                deallocate(temporaryOb)
                allocate(temporaryOb(1:nObstacle))
                temporaryOb = obstacleNodes
                temp = 1
                exit
             end if
          end do
          if (temp .ne. 1) then
             nObstacle = nObstacle+2
             allocate(obstacleNodes(1:nObstacle))
             do j = 1,nObstacle-2
                obstacleNodes(j) = temporaryOb(j)
             end do
             obstacleNodes(nObstacle-1) = a
             obstacleNodes(nObstacle)   = b
             deallocate(temporaryOb)
             allocate(temporaryOb(1:nObstacle))
             temporaryOb = obstacleNodes
          end if
       end if
       tempOb = b
       deallocate(obstacleNodes)
   end if
 end do
 allocate(inletNodes(1:nInlet),outletNodes(1:nOutlet))
 allocate(wallBottomNodes(1:nWall_bottom), wallTopNodes(1:nWall_top))
 if (obstacle .eq. 1) then
    allocate(obstacleNodes(1:nObstacle))
 end if
 inletNodes = temporaryIn
 outletNodes = temporaryOut
 wallBottomNodes = temporaryWallB
 wallTopNodes = temporaryWallT
 nWall = nWall_bottom+nWall_top

 if (obstacle .eq. 1) then
 
    obstacleNodes = temporaryOb
 
    allocate(xObstacle(1:nObstacle),yObstacle(1:nObstacle),zObstacle(1:nObstacle))
    allocate(rnx(1:nObstacle), rny(1:nObstacle), lengthOb(1:nObstacle))
 
    do i = 1,nObstacle
       xObstacle(i) = xn(obstacleNodes(i))
       yObstacle(i) = yn(obstacleNodes(i))
       zObstacle(i) = zn(obstacleNodes(i))
       flagn(obstacleNodes(i)) = 10
    end do
 
    ! I nodi dell'ostacolo sono ordinati in senso ANTIORARIO
    ! e le normali devono essere uscenti dall'ostacolo, ossia entranti nei corripondenti elementi triangolari.

    do i = 1,nObstacle

       j = obstacleNodes(i)
       if ( i .ne. nObstacle) then
          k = obstacleNodes(i+1)
       else
          k = obstacleNodes(1)
       end if

       lengthOb(i) = sqrt(((xn(j)-xn(k))**2.d0)+((yn(j)-yn(k))**2.d0))

       rnx(i) =  (yn(k)-yn(j))/lengthOb(i)
       rny(i) = -(xn(k)-xn(j))/lengthOb(i)

    end do

 end if
 
 write(50,*) 'nObstacle =', nObstacle
 write(50,*) 'nWall     =', nWall
 write(50,*) 'nInlet    =', nInlet 
 write(50,*) 'nOutlet   =', nOutlet
 write(50,*) '==================================='
 allocate(inletBufferNodes(1:nInlet), outletBufferNodes(1:nOutlet))
 allocate(wallNodes(1:nWall))
 allocate(xIn(1:nInlet),yIn(1:nInlet),zIn(1:nInlet))
 allocate(xOut(1:nOutlet),yOut(1:nOutlet),zOut(1:nOutlet))
 allocate(xWall(1:nWall),yWall(1:nWall),zWall(1:nWall))
 do i = 1,nElements

 ! se i nodi degli elementi triangolari sono letti in senso ORARIO:
 ! read(10,*) xxx, xxx, xxx, xxx, xxx, conn(1,i), conn(2,i), conn(3,i)
 ! se i nodi degli elementi triangolari sono letti in senso ANTIORARIO:
 ! read(10,*) xxx, xxx, xxx, xxx, xxx, conn(1,i), conn(3,i), conn(2,i)
 ! in modo tale da ricondursi al senso ORARIO.
 
    read(10,*) xxx, xxx, xxx, xxx, xxx, conn(1,i), conn(2,i), conn(3,i)
 end do
 close(10)
 do i = 1,nWall_bottom
    wallNodes(i) = wallBottomNodes(i)
    flagn(wallNodes(i)) = 4
 end do
 j = 0
 do i = nWall_bottom+1,nWall
    j = j+1
    wallNodes(i) = wallTopNodes(j)
    flagn(wallNodes(i)) = 7
 end do
 do i = 1,nInlet
    xIn(i) = xn(inletNodes(i))
    yIn(i) = yn(inletNodes(i))
    zIn(i) = zn(inletNodes(i))
 end do
 do i = 1,nOutlet
    xOut(i) = xn(outletNodes(i))
    yOut(i) = yn(outletNodes(i))
    zOut(i) = zn(outletNodes(i))
 end do
 do i = 1,nWall
    xWall(i) = xn(wallNodes(i))
    yWall(i) = yn(wallNodes(i))
    zWall(i) = zn(wallNodes(i))
 end do
 do i = 1,nInlet-1
    a = i
    do j = i+1,nInlet
       if (yIn(j) .gt. yIn(a)) then
          a = j
       end if
    end do
    if (a .ne. i) then
       a1 = xIn(i)
       xIn(i) = xIn(a)
       xIn(a) = a1
       a2 = yIn(i)
       yIn(i) = yIn(a)
       yIn(a) = a2
       a3 = zIn(i)
       zIn(i) = zIn(a)
       zIn(a) = a3
       tempIn = inletNodes(i)
       inletNodes(i) = inletNodes(a)
       inletNodes(a) = tempIn
    end if
 end do
 do i = 1,nOutlet-1
    a = i
    do j = i+1,nOutlet
       if (yOut(j) .lt. yOut(a)) then
          a = j
       end if
    end do
    if (a .ne. i) then
       a1 = xOut(i)
       xOut(i) = xOut(a)
       xOut(a) = a1
       a2 = yOut(i)
       yOut(i) = yOut(a)
       yOut(a) = a2
       a3 = zOut(i)
       zOut(i) = zOut(a)
       zOut(a) = a3
       tempOut = outletNodes(i)
       outletNodes(i) = outletNodes(a)
       outletNodes(a) = tempOut
    end if
 end do
 if (AddBuffer .eq. 1) then
    write(50,*) '        .....upgrade.....          '
    write(50,*) '    .....creating buffers.....     '
    write(50,*) '==================================='
    bufferNodes = nInlet+nOutlet
    nNodesTot = nNodes + bufferNodes
    nElementsTot = nElements + (nInlet-1)*2 + (nOutlet-1)*2
    write(50,*) 'number of nodes:'
    write(50,*) nNodesTot
    write(50,*) '==================================='
    write(50,*) 'number of elements:'
    write(50,*) nElementsTot
    write(50,*) '==================================='
    allocate(x(1:nNodesTot),y(1:nNodesTot),z(1:nNodesTot),flag(1:nNodesTot))
    allocate(conn_tot(1:3,1:nElementsTot))
    do i = 1,nNodes
       x(i) = xn(i)
       y(i) = yn(i)
       z(i) = zn(i)
       flag(i) = flagn(i)
    end do
    j = 0
    do i = nNodes+1,nNodes+nInlet
       j = j+1
       inletBufferNodes(j) = i
       x(i) = xIn(j)-buffer
       y(i) = yIn(j)
       z(i) = zIn(j)
       if ( j .eq. 1 .or. j .eq. nInlet) then
          flag(i) = 5
       else
          flag(i) = 2
       end if
    end do
    j = 0
    do i = nNodes+nInlet+1,nNodesTot
       j = j+1
       outletBufferNodes(j) = i
       x(i) = xOut(j)+buffer
       y(i) = yOut(j)
       z(i) = zOut(j)
       if ( j .eq. 1 .or. j .eq. nOutlet) then
          flag(i) = 6
       else
          flag(i) = 3
       end if
    end do
    do i = 1,nElements
       conn_tot(1,i) = conn(1,i)
       conn_tot(2,i) = conn(2,i)
       conn_tot(3,i) = conn(3,i)
    end do
    k = 0
    do i = 1,(nInlet-1),1
       k = k+1
       j = nElements+1+2*(k-1)
       if (2*i .gt. nInlet) then
          conn_tot(1,j) = inletNodes(i)
          conn_tot(2,j) = inletBufferNodes(i+1)
          conn_tot(3,j) = inletBufferNodes(i)
          conn_tot(1,j+1) = inletNodes(i)
          conn_tot(2,j+1) = inletNodes(i+1)
          conn_tot(3,j+1) = inletBufferNodes(i+1)
      else
          conn_tot(1,j) = inletNodes(i+1)
          conn_tot(2,j) = inletBufferNodes(i+1)
          conn_tot(3,j) = inletBufferNodes(i)
          conn_tot(1,j+1) = inletNodes(i)
          conn_tot(2,j+1) = inletNodes(i+1)
          conn_tot(3,j+1) = inletBufferNodes(i)
      end if
    end do
    do i = 1,nOutlet-1
       k = k+1
       j = nElements+1+2*(k-1)
       if (2*i .gt. nOutlet) then
          conn_tot(1,j) = outletNodes(i+1)
          conn_tot(2,j) = outletBufferNodes(i+1)
          conn_tot(3,j) = outletNodes(i)
          conn_tot(1,j+1) = outletNodes(i)
          conn_tot(2,j+1) = outletBufferNodes(i+1)
          conn_tot(3,j+1) = outletBufferNodes(i)
      else
          conn_tot(1,j) = outletNodes(i+1)
          conn_tot(2,j) = outletBufferNodes(i)
          conn_tot(3,j) = outletNodes(i)
          conn_tot(1,j+1) = outletNodes(i+1)
          conn_tot(2,j+1) = outletBufferNodes(i+1)
          conn_tot(3,j+1) = outletBufferNodes(i)
      end if
    end do

    close(50)

    nNodes = nNodesTot
    nElements = nElementsTot

 else

    write(50,*) '    .....no buffers added.....     '
    allocate(x(1:nNodes),y(1:nNodes),z(1:nNodes),flag(1:nNodes))
    allocate(conn_tot(1:3,1:nElements))
    do i = 1,nNodes
       x(i) = xn(i)
       y(i) = yn(i)
       z(i) = zn(i)
       flag(i) = flagn(i)
    end do
    do i = 1,nInlet
       j = inletNodes(i)
       if ( i .eq. 1 .or. i .eq. nInlet) then
          flag(j) = 5
       else
          flag(j) = 2
       end if
    end do
    do i = 1,nOutlet
       j = outletNodes(i)
       if ( i .eq. 1 .or. i .eq. nOutlet) then
          flag(j) = 6
       else
          flag(j) = 3
       end if
    end do
    do i = 1,nElements
       conn_tot(1,i) = conn(1,i)
       conn_tot(2,i) = conn(2,i)
       conn_tot(3,i) = conn(3,i)
    end do

    close(50)

 end if

end subroutine mesh
