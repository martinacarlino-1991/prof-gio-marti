 subroutine aero

!=====================================!
! Aerodynamic forces and coefficients !
!=====================================!

 use shared

 Lift = 0.d0
 Drag = 0.d0

 do i = 1,nObstacle

    j = obstacleNodes(i)
    if ( i .ne. nObstacle) then
       k = obstacleNodes(i+1)
    else
       k = obstacleNodes(1)
    end if

    sxxl = 0.5d0*(sxx(j)+sxx(k))
    syyl = 0.5d0*(syy(j)+syy(k))
    sxyl = 0.5d0*(sxy(j)+sxy(k))
    syxl = 0.5d0*(syx(j)+syx(k))
    rpress = (rhot(j)+rhot(k))*0.5d0/csq

!   Sforzo tangenziale sulla superficie dell'ostacolo
    stn = -sxxl*rnx(i)*rny(i)-sxyl*rny(i)*rny(i)+syxl*rnx(i)*rnx(i)+syyl*rnx(i)*rny(i)

! Frictional Force
    lift_v(i) =  stn*rnx(i)*lengthOb(i)
    drag_v(i) = -stn*rny(i)*lengthOb(i)
!   drag_v(i) = (sxxl*rnx(i)+sxyl*rny(i))*lengthOb(i)
!   lift_v(i) = (syyl*rny(i)+syxl*rnx(i))*lengthOb(i)

! Pressure Force
    lift_p(i) = -rpress*rny(i)*lengthOb(i)
    drag_p(i) = -rpress*rnx(i)*lengthOb(i)

! Aerodynamic Forces
    Lift = Lift + lift_v(i) + lift_p(i)
    Drag = Drag + drag_v(i) + drag_p(i)

! Pressure Coefficient
    CP(i) = ((rhot(j)-rho0)*cs**2.d0)/(0.5d0*rho0*(u0*u0))

! Skin Friction Coefficient
    CF(i) = stn/(0.5d0*rho0*(u0*u0))

 end do

 CL = Lift/(0.5d0*rho0*(u0*u0)*length)
 CD = Drag/(0.5d0*rho0*(u0*u0)*length)

 end subroutine aero
