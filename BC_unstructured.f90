 subroutine BC_unstructured

 use shared

 !======================================== INLET

 if (flag(i) .eq. 2 .or. flag(i) .eq. 5 ) then
    ncontin = ncontin+1
    nrcont  = inletNodes(ncontin)
    if     (inletBC_un .eq. 1) then
       ! fixed constant velocity, pressure gradient = 0
       rhot(i)   = rhot(nrcont)
       velt(1,i) = u0
       velt(2,i) = v0
    elseif (inletBC_un .eq. 2) then
       ! fixed parabolic velocity, pressure gradient = 0
       rhot(i) = rhot(nrcont)
       velt(1,i) = -(6.d0*u0/(h*h))*(y(i)*y(i)+(Y_min**2.d0-Y_max**2.d0)*y(i)/h+Y_max*Y_min)
       velt(2,i) = v0
    elseif (inletBC_un .eq. 0) then
       ! periodic
       rhot(i)   = rhot(outletNodes(nOutlet+1-ncontin))
       velt(1,i) = velt(1,outletNodes(nOutlet+1-ncontin))
       velt(2,i) = velt(2,outletNodes(nOutlet+1-ncontin))
    end if
 end if

 !======================================== OUTLET

 if (flag(i) .eq. 3 .or. flag(i) .eq. 6 ) then
    ncontout = ncontout+1
    nrcont  = outletNodes(ncontout)
    if     (outletBC_un .eq. 3) then
       ! fixed constant pressure, velocity gradient = 0 
       rhot(i)   = rho0
       velt(1,i) = velt(1,nrcont)
       velt(2,i) = velt(2,nrcont)
    elseif (outletBC_un .eq. 0) then
       ! periodic
       rhot(i)   = rhot(inletNodes(nInlet+1-ncontout))
       velt(1,i) = velt(1,inletNodes(nInlet+1-ncontout))
       velt(2,i) = velt(2,inletNodes(nInlet+1-ncontout))
    end if
 end if

 !======================================== TOP_WALL

 if (flag(i) .eq. 7) then
     if     (TopWallBC_un .eq. 4) then
        ! no-slip condition
        velt(1,i) = 0.d0
        velt(2,i) = 0.d0
     elseif (TopWallBC_un .eq. 7) then
        ! free-slip condition
        velt(2,i) = 0.d0
     end if
  end if

 !======================================== BOTTOM_WALL

 if (flag(i) .eq. 4) then
    if     (BottomWallBC_un .eq. 4) then
       ! no-slip condition
       velt(1,i) = 0.d0
       velt(2,i) = 0.d0
    elseif (BottomWallBC_un .eq. 7) then
       ! free-slip condition
       velt(2,i) = 0.d0
    end if
 end if

 !======================================== OBSTACLE

 if (flag(i) .eq. 10) then
    ! no-slip condition
    velt(1,i) = 0.d0
    velt(2,i) = 0.d0
 end if

 end subroutine BC_unstructured
