module shared

 implicit none
 integer, parameter           :: dp = selected_real_kind(15,307)

 integer, parameter           :: n_th = 1

 real(kind=dp), parameter     :: pi = 3.14159d0
 real(kind=dp)                :: kvisc, ril
 real(kind=dp)                :: massIn, massOut, errTOT, errInOut, rmass
 integer                      :: it, iter, it0
 real(kind=dp)                :: Y_min, Y_max, h
 real(kind=dp)                :: u0, v0, rho0, Re, Mach, g
 integer                      :: a, b, i, j, k, l, m, ii, jj, str1, str2, tag
 integer                      :: tempIn, tempOut, tempWallB, tempWallT, tempOb, temp
 integer                      :: nNodes, nNodesTot, bufferNodes, nElements, nElementsTot, nLines, totElements
 real(kind=dp)                :: a1, a2, a3
 real(kind=dp)                :: length_Int(1:3,1:2), length_v(1:3), surface(1:3)
 real(kind=dp)                :: tot_surf
 integer                      :: w0, w1, w2
 real(kind=dp)                :: xv0, yv0, xv1, yv1, xv2, yv2, xv, yv, detA, detB, detC, detD, detE, vA, vB
 integer                      :: check, xxx
 character                    :: nothing, id
 character(280)               :: meshName
 integer, parameter           :: maxc = 10
 integer                      :: nWall, nWall_bottom, nWall_top, nInlet, nOutlet, nObstacle
 real(kind=dp)                :: buffer
 integer                      :: AddBuffer
 integer                      :: inletBC_un, outletBC_un, TopWallBC_un, BottomWallBC_un
 real(kind=dp)                :: xp(1:4), yp(1:4)
 real(kind=dp), allocatable   :: xn(:), yn(:), zn(:), x(:), y(:), z(:), x_temp(:), y_temp(:), y_0(:)
 real(kind=dp), allocatable   :: xIn(:), yIn(:), zIn(:)
 real(kind=dp), allocatable   :: xOut(:), yOut(:), zOut(:)
 real(kind=dp), allocatable   :: xWall(:), yWall(:), zWall(:)
 real(kind=dp), allocatable   :: xObstacle(:), yObstacle(:), zObstacle(:)
 integer, allocatable         :: conn(:,:), conn_tot(:,:)
 integer, allocatable         :: flagn(:), flag(:)
 integer, allocatable         :: temporaryIn(:), temporaryOut(:), temporaryWallB(:), temporaryWallT(:), temporaryOb(:)
 integer, allocatable         :: inletNodes(:), outletNodes(:)
 integer, allocatable         :: wallNodes(:), wallBottomNodes(:), wallTopNodes(:)
 integer, allocatable         :: obstacleNodes(:)
 integer, allocatable         :: inletBufferNodes(:), outletBufferNodes(:)
 integer                      :: dump, noutput
 real(kind=dp)                :: deltat, length
 real(kind=dp)                :: c_ret(2,9), w_eq(9), cs
 real(kind=dp), allocatable   :: feq(:,:), fneq_un(:,:)
 real(kind=dp), allocatable   :: xvc(:,:), yvc(:,:)
 real(kind=dp), allocatable   :: norm(:,:,:), normtr(:,:,:)
 real(kind=dp), allocatable   :: lengthl(:,:), lengtht(:,:), lengthr(:,:)
 real(kind=dp), allocatable   :: rnx(:), rny(:), lengthOb(:)
 real(kind=dp), allocatable   :: surf(:,:,:), surft(:), surf_inv(:)
 real(kind=dp), allocatable   :: sxx(:), sxy(:), syx(:), syy(:)
 real(kind=dp), allocatable   :: fpop(:,:), rhot(:), velt(:,:)
 real(kind=dp), allocatable   :: fpop_0(:,:), rhot_0(:), velt_0(:,:)
 real(kind=dp), allocatable   :: c_cd(:), c_sd(:,:), c_c(:,:), c_s(:,:,:)
 integer, allocatable         :: connections(:,:), nnodi(:)
 real(kind=dp)                :: qxx(9), qxy(9), qyy(9), qyx(9)
 real(kind=dp)                :: rinv_9, rinv_12
 integer                      :: ncdrag, obstacle
 integer                      :: feq_cubic
 real(kind=dp)                :: lato_a, lato_b, lato_c, semip, sup
 real(kind=dp)                :: fpopeq
 integer                      :: ncon(3), ncontin, ncontout, nrcont
 real(kind=dp)                :: scont, qcont
 real(kind=dp)                :: Lift, Drag, CL, CD, sxxl, sxyl, syyl, stn
 real(kind=dp)                :: inv_ril
 real(kind=dp)                :: coll, stream
 real(kind=dp)                :: xloc(3), yloc(3)
 integer                      :: k1, k2, k3, n1, n2, n3, np
 real(kind=dp)                :: nr1, nr2, r_nc
 real(kind=dp)                :: rpress, csq
 real(kind=dp), allocatable   :: lift_p(:), drag_p(:), lift_v(:), drag_v(:), CP(:), CF(:)

!============================================================================================================
! OUTPUT_PARAVIEW variables
!============================================================================================================
  integer, allocatable        :: cn(:), off(:), tp(:)
  character(280)              :: string, command
  character(280)              :: fn = ' '
  character(280)              :: filename_1 = ' '
  character(280)              :: filename
  character(280)              :: velocity, density, shear, cell_number, coordinates
  character(280)              :: connectivity, offsets, types
  character(280)              :: NN, NC
  real(kind=dp)               :: sizereal
  integer                     :: sizeint
  integer                     :: ss1, ss2, deltaOut, itOut
  integer                     :: offset_velocity, offset_density, offset_cell_number, offset_coordinates
  integer                     :: offset_connectivity, offset_offsets, offset_types
  integer                     :: offset_shear

end module shared
