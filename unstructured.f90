 subroutine unstructured

 use shared
 use omp_lib

 call omp_set_num_threads(n_th)

 ncontout = 0
 ncontin  = 0
 nrcont   = 0

 !$omp parallel do private(scont,qcont,i,j)
 do i = 1,nNodes

    do j = 1,9
       fpop_0(j,i) = fpop(j,i)
       scont = c_ret(1,j)*velt(1,i)+c_ret(2,j)*velt(2,i)
       qcont = velt(1,i)**2.d0+velt(2,i)**2.d0
       if (feq_cubic .eq. 1) then
           feq(j,i) = w_eq(j)*rhot(i)*(1.d0+3.d0*scont+4.5d0*scont*scont-1.5d0*qcont+&
                    & 4.5d0*scont*scont*scont-4.5d0*scont*qcont)
       else
           feq(j,i) = w_eq(j)*rhot(i)*(1.d0+3.d0*scont+4.5d0*scont*scont-1.5d0*qcont)
       end if

       fneq_un(j,i) = fpop_0(j,i)-feq(j,i)

    end do

 end do
 !$omp end parallel do 

 !$omp parallel do private(i,j,k,coll,stream)
 do i = 1,nNodes

    do j = 1,9

    coll  = 0.d0
    stream = 0.d0

    do k = 1,nnodi(i)
       coll = coll+c_c(k,i)*fneq_un(j,connections(i,k))
       stream = stream+c_s(k,j,i)*fpop_0(j,connections(i,k))
    end do

    coll = coll+c_cd(i)*fneq_un(j,i)
    stream = stream+c_sd(j,i)*fpop_0(j,i)

    fpop(j,i) = fpop_0(j,i)+surf_inv(i)*(inv_ril*coll-stream)

    end do

    rhot(i) = fpop(1,i)+fpop(2,i)+fpop(3,i)+fpop(4,i)+fpop(5,i)+fpop(6,i)+fpop(7,i)+fpop(8,i)+fpop(9,i)
    velt(1,i) = (1.d0/rhot(i))*(fpop(2,i)-fpop(4,i)+fpop(6,i)-fpop(7,i)-fpop(8,i)+fpop(9,i))
    velt(2,i) = (1.d0/rhot(i))*(fpop(3,i)-fpop(5,i)+fpop(6,i)+fpop(7,i)-fpop(8,i)-fpop(9,i))

 end do
 !$omp end parallel do 

 end subroutine unstructured
